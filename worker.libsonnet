{
  local k = import 'github.com/jsonnet-libs/k8s-libsonnet/main.libsonnet',
  local Container = k.core.v1.container,
  local Deployment = k.apps.v1.deployment,

  local workerSchema = {
    type: 'object',
    properties: {
      image: { type: 'string', title: 'Container image', description: 'Location of the image', },
      cmd: { type: 'string', title: 'Container command', description: 'The command to run in container or the first argument passed the the entrypoint script', },
      args: { type: 'string', title: 'Container command arguments', description: 'Argumements to pass to the command or second and further arguments passed the the entrypoint script', },
      env: { type: 'object', title: 'ENV variables', description: 'String literals to inject as ENV variables', },  /* TODO how to specify the schema to represent props inside env object? */
      replicas: { type: 'number', title: 'Pod replicas', description: 'Run multiple parallel instances (replicas) of the container', },
    },
    required: [
      'image',
    ],
  },

  libraryMeta: {
    thisFile: std.thisFile,
    templates: {
      Worker: {
        template: 'Worker',
        schema: workerSchema + {
          title: 'Worker process',
          description: 'Deployment w/o serice',
        },
      },
    },
  },

  Worker:: {
    templateMeta:: $.libraryMeta.templates.Worker,
    factory:: function(squareName, square) {
      assert std.objectHas(square, 'image'),
      assert ! std.objectHas(square, 'replicas') || std.objectHas(square, 'replicas') && std.isNumber(square.replicas): 'replicas must be a number',

      deployment: Deployment.new(squareName, containers=[
        Container.new(squareName, square.image)
        + (if std.objectHas(square, 'cmd') then Container.withCommand(std.split(square.cmd, ' ')) else {})
        + (if std.objectHas(square, 'args') then Container.withArgs(std.split(square.args, ' ')) else {})
        + (if std.objectHas(square, 'env') then
          Container.withEnvMap(square.env) else {}
        )
      ])
      + (if std.objectHas(square, 'replicas') then Deployment.spec.withReplicas(square.replicas) else {})
    },
  },
}
